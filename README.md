<p align="center">
<img src="https://github.com/vitorbritto/kickstarts/raw/master/source/kickstarts.jpg" alt="Kickstarts" width="500">
</p>


## Getting Started

Install with [npm](https://www.npmjs.com/):

```sh
$ npm i -g kickstarts
```


## Usage

```sh
$ kick start
```

**then...**

1. Choose the kickstart
2. Set the directory name


## License

[MIT License](http://kickstarts.mit-license.org/) © Kickstarts



